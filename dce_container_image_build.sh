#!/bin/bash
set -euo pipefail

. dce_container_image_utils.sh

dce_determine_image_variables

export CPATH=includes:/usr/local/share/dce

[ -n "${!pip_package_override_*}" ] && declare -p "${!pip_package_override_@}" > .env.pip_package_overrides

dce_loop buildah bud \
    --build-arg "IMAGE_DESCRIPTION=$IMAGE_NAME ${CI_PIPELINE_ID:-} ${IMAGE_TAGS[*]}" \
    --file "builds/$IMAGE_NAME.in" \
    --tag "$IMAGE_NAME" \
    .

if [ -v CI_REGISTRY ]; then
    echo "$CI_REGISTRY_PASSWORD" | buildah login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    echo "Uploading to $CI_REGISTRY_IMAGE/$IMAGE_NAME:p-$CI_PIPELINE_ID"
    dce_loop buildah push "$IMAGE_NAME" "docker://$CI_REGISTRY_IMAGE/$IMAGE_NAME:p-$CI_PIPELINE_ID"
fi

exit 0
