#!/bin/bash
set -euo pipefail

. dce_container_image_utils.sh

dce_determine_image_variables

if [ -v CI_REGISTRY ]; then
    echo "$CI_REGISTRY_PASSWORD" | skopeo login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    for IMAGE_TAG in "${IMAGE_TAGS[@]}"; do
        echo "Uploading to $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG"
        dce_loop skopeo copy --all "docker://$CI_REGISTRY_IMAGE/$IMAGE_NAME:p-$CI_PIPELINE_ID" "docker://$CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG"
    done
fi

exit 0
